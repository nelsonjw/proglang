#!/usr/bin/env ruby

require 'minitest/autorun'
require 'compare.rb'

class LaterTests < MiniTest::Test
  def test_cralf
    compare 'cralf', 'Cralf1.java', 1
    compare 'cralf', 'Cralf2.java', 1
    compare 'cralf', 'Cralf3.java', 1
    compare 'cralf', 'WhyThisCralf.java', 1
    compare 'cralf', 'NoCralf.java', 0
  end

  def test_export
    compare 'export', 'Export1.java', 1
    compare 'export', 'Export2.java', 1
    compare 'export', 'NoExport.java', 0
    compare 'export', 'HundredPercent.java', 1
  end

  def test_ymd
    compare 'ymd', 'life.cal', 0
    compare 'ymd', 'dates1.txt', 0
    compare 'ymd', 'dates2.txt', 0
    compare 'ymd', 'dates3.txt', 0
  end

  def test_posttruth
    compare 'posttruth', 'Bools.java', 1
    compare 'posttruth', 'NoBools.java', 0
    compare 'posttruth', 'LoverOfFalse.java', 1
    compare 'posttruth', 'BoolOkay.java', 0
  end
end
