#!/usr/bin/env ruby

G = Random.new

def generate_int_list(min, max)
  n = G.rand 20
  '[' + (0..n).map { min + G.rand(max - min + 1) }.join(', ') + ']'
end

def generate_direction
  %w{North South East West}[G.rand 4]
end

def generate_string

end

def randomBullsCows n
  "0123456789".chars.shuffle.take(n).join
end

# puts generate_int_list -100, 100

# Move
# print "(#{G.rand(200)}, #{G.rand(200)}) ["
# print (0..G.rand(50)).map { generate_direction }.join(', ')
# puts "]"

# withins, isFree
# n = G.rand(100)
# intervals = (0..G.rand(1000)).map do
  # lo = G.rand(50)
  # hi = lo + G.rand(50)
  # "(#{lo},#{hi})"
# end.join(', ')
# puts "#{n} [#{intervals}]"
# puts "[#{intervals}]"

# n = 5
# puts "\"#{randomBullsCows(n)}\" \"#{randomBullsCows(n)}\""

# puts "#{G.rand(100)} #{G.rand(7)}"

# binaryToDecimal
# puts (0..20).map { '"' + (0..G.rand(15)).map { G.rand(2) }.join + '"' }.join(', ')

# decimalToBinary', decimalToBinary
puts "[#{(0..500).map { G.rand(256) }.join(', ')}]"

