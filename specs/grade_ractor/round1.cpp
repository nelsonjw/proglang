#include <iostream>
#include <map>
#include <string>
#include <vector>
#include <gtest/gtest.h>

#include "Rectangle.h"
#include "Button.h"
#include "List.h"
#include "Checkbox.h"
#include "Slider.h"
#include "Label.h"

using std::map;
using std::vector;
using std::string;

TEST(Rectangle, Getters) {
  Rectangle r(0, 3, 10, 15);
  ASSERT_EQ(0, r.GetLeft());
  ASSERT_EQ(3, r.GetRight());
  ASSERT_EQ(10, r.GetTop());
  ASSERT_EQ(15, r.GetBottom());

  r = Rectangle(10, 20, 100, 102);
  ASSERT_EQ(10, r.GetLeft());
  ASSERT_EQ(20, r.GetRight());
  ASSERT_EQ(100, r.GetTop());
  ASSERT_EQ(102, r.GetBottom());
}

TEST(Rectangle, WidthHeight) {
  Rectangle r(0, 3, 10, 15);
  ASSERT_EQ(4, r.GetWidth());
  ASSERT_EQ(6, r.GetHeight());
  r = Rectangle(10, 20, 100, 102);
  ASSERT_EQ(11, r.GetWidth());
  ASSERT_EQ(3, r.GetHeight());
}

TEST(Rectangle, Setters) {
  Rectangle r(17, 17, 19, 19);
  ASSERT_EQ(17, r.GetLeft());
  ASSERT_EQ(17, r.GetRight());
  ASSERT_EQ(19, r.GetTop());
  ASSERT_EQ(19, r.GetBottom());
  ASSERT_EQ(1, r.GetWidth());
  ASSERT_EQ(1, r.GetHeight());

  r.SetLeft(12);
  ASSERT_EQ(12, r.GetLeft());
  ASSERT_EQ(6, r.GetWidth());

  r.SetLeft(13);
  ASSERT_EQ(13, r.GetLeft());
  ASSERT_EQ(5, r.GetWidth());

  r.SetRight(24);
  ASSERT_EQ(24, r.GetRight());
  ASSERT_EQ(12, r.GetWidth());

  r.SetBottom(45);
  ASSERT_EQ(45, r.GetBottom());
  ASSERT_EQ(27, r.GetHeight());

  r.SetBottom(31);
  ASSERT_EQ(31, r.GetBottom());
  ASSERT_EQ(13, r.GetHeight());

  r.SetTop(28);
  ASSERT_EQ(28, r.GetTop());
  ASSERT_EQ(4, r.GetHeight());
}

TEST(Rectangle, Contains) {
  Rectangle rect(-5, 6, 10, 13);
  for (int r = -30; r <= 40; ++r) {
    for (int c = -20; c <= 50; ++c) {
      bool expected = r >= 10 && r <= 13 && c >= -5 && c <= 6;
      ASSERT_EQ(expected, rect.Contains(c, r)) << "I asked rectangle spanning (-5, 10) to (6, 13) if it contained (" << c << ", " << r << "), but I didn't get back the answer I expected.";
    }
  }
}

TEST(Button, Rectangle) {
  Button b(10, 30, "submit");
  ASSERT_EQ(10, b.GetLeft());
  ASSERT_EQ(15, b.GetRight());
  ASSERT_EQ(30, b.GetTop());
  ASSERT_EQ(30, b.GetBottom());
  for (int r = -30; r <= 40; ++r) {
    for (int c = -20; c <= 50; ++c) {
      bool expected = r == 30 && c >= 10 && c <= 15;
      ASSERT_EQ(expected, b.Contains(c, r)) << "I asked a button at (10, 30) with text \"submit\" if it contained (" << c << ", " << r << "), but I didn't get back the answer I expected.";
    }
  }

  b = Button(113, 92, "clickety-click");
  ASSERT_EQ(113, b.GetLeft());
  ASSERT_EQ(126, b.GetRight());
  ASSERT_EQ(92, b.GetTop());
  ASSERT_EQ(92, b.GetBottom());
  for (int r = -30; r <= 130; ++r) {
    for (int c = -20; c <= 200; ++c) {
      bool expected = r == 92 && c >= 113 && c <= 126;
      ASSERT_EQ(expected, b.Contains(c, r)) << "I asked a button at (113, 126) with text \"clickety-click\" if it contained (" << c << ", " << r << "), but I didn't get back the answer I expected.";
    }
  }
}

TEST(Button, Listening) {
  int nclicks1 = 0;
  Button b(3, 9, "increment");
  b.AddListener([&]() {
    ++nclicks1;
  });

  b.OnMouseClick(0, 0);
  ASSERT_EQ(1, nclicks1) << "I tried calling Button::OnMouseClick, but it didn't call the registered listener as I expected.";
  b.OnMouseClick(1, 0);
  ASSERT_EQ(2, nclicks1) << "I tried calling Button::OnMouseClick, but it didn't call the registered listener as I expected.";;

  int nclicks2 = 0;
  b.AddListener([&]() {
    ++nclicks2;
  });

  b.OnMouseClick(1, 0);
  ASSERT_EQ(3, nclicks1) << "I tried calling Button::OnMouseClick, but it didn't call the registered listener as I expected.";;
  ASSERT_EQ(1, nclicks2) << "I tried calling Button::OnMouseClick, but it didn't call the registered listener as I expected.";;
  b.OnMouseClick(2, 0);
  ASSERT_EQ(4, nclicks1) << "I tried calling Button::OnMouseClick, but it didn't call the registered listener as I expected.";;
  ASSERT_EQ(2, nclicks2) << "I tried calling Button::OnMouseClick, but it didn't call the registered listener as I expected.";;
}

TEST(Label, Rectangle) {
  Label l(20, 32, "general preferences");
  ASSERT_EQ(20, l.GetLeft());
  ASSERT_EQ(38, l.GetRight());
  ASSERT_EQ(32, l.GetTop());
  ASSERT_EQ(32, l.GetBottom());
  for (int r = -30; r <= 40; ++r) {
    for (int c = -20; c <= 50; ++c) {
      bool expected = r == 32 && c >= 20 && c <= 38;
      ASSERT_EQ(expected, l.Contains(c, r)) << "I asked a label at (20, 32) with text \"general preferences\" if it contained (" << c << ", " << r << "), but I didn't get back the answer I expected.";
    }
  }

  l = Label(145, 560, "dietary options");
  ASSERT_EQ(145, l.GetLeft());
  ASSERT_EQ(159, l.GetRight());
  ASSERT_EQ(560, l.GetTop());
  ASSERT_EQ(560, l.GetBottom());
  for (int r = -200; r <= 200; ++r) {
    for (int c = -200; c <= 700; ++c) {
      bool expected = r == 560 && c >= 145 && c <= 158;
      ASSERT_EQ(expected, l.Contains(c, r)) << "I asked a label at (145, 560) with text \"dietary options\" if it contained (" << c << ", " << r << "), but I didn't get back the answer I expected.";
    }
  }
}

TEST(Label, SetText) {
  Label l(0, 0, "foo");

  l.SetText("stereo prefs");
  ASSERT_EQ(0, l.GetLeft());
  ASSERT_EQ(11, l.GetRight());
  ASSERT_EQ(0, l.GetTop());
  ASSERT_EQ(0, l.GetBottom());

  l.SetText("president");
  ASSERT_EQ(0, l.GetLeft());
  ASSERT_EQ(8, l.GetRight());
  ASSERT_EQ(0, l.GetTop());
  ASSERT_EQ(0, l.GetBottom());

  l.SetText("X");
  ASSERT_EQ(0, l.GetLeft());
  ASSERT_EQ(0, l.GetRight());
  ASSERT_EQ(0, l.GetTop());
  ASSERT_EQ(0, l.GetBottom());
}

TEST(Checkbox, Rectangle) {
  Checkbox b(89, 45, "carnivore");
  ASSERT_EQ(89, b.GetLeft());
  ASSERT_EQ(101, b.GetRight());
  ASSERT_EQ(45, b.GetTop());
  ASSERT_EQ(45, b.GetBottom());
  for (int r = -30; r <= 40; ++r) {
    for (int c = -20; c <= 50; ++c) {
      bool expected = r == 45 && c >= 89 && c <= 101;
      ASSERT_EQ(expected, b.Contains(c, r)) << "I asked a checkbox at (89, 45) with text \"carnivore\" if it contained (" << c << ", " << r << "), but I didn't get back the answer I expected.";
    }
  }

  b = Checkbox(50, 51, "window");
  ASSERT_EQ(50, b.GetLeft());
  ASSERT_EQ(59, b.GetRight());
  ASSERT_EQ(51, b.GetTop());
  ASSERT_EQ(51, b.GetBottom());
  for (int r = -30; r <= 40; ++r) {
    for (int c = -20; c <= 50; ++c) {
      bool expected = r == 51 && c >= 50 && c <= 59;
      ASSERT_EQ(expected, b.Contains(c, r)) << "I asked a checkbox at (50, 59) with text \"window\" if it contained (" << c << ", " << r << "), but I didn't get back the answer I expected.";
    }
  }
}

TEST(Checkbox, IsChecked) {
  Checkbox b(13, 28, "braces on same line");
  ASSERT_FALSE(b.IsChecked()) << "I called Checkbox::IsChecked() on a freshly constructed checkbox, but I didn't get the back the result I expected.";
  b.IsChecked(false);
  ASSERT_FALSE(b.IsChecked()) << "I called Checkbox::IsChecked() on a checkbox explicitly checked false with IsChecked(false), but I didn't get the back the result I expected.";
  b.IsChecked(true);
  ASSERT_TRUE(b.IsChecked()) << "I called Checkbox::IsChecked() on a checkbox explicitly checked true with IsChecked(true), but I didn't get the back the result I expected.";
}

TEST(Checkbox, Listening) {
  Checkbox b(8, 17, "mild");
  bool listener_check = false;
  b.AddListener([&](bool is_checked) {
    listener_check = is_checked;
  });
  b.OnMouseClick(0, 0);
  ASSERT_TRUE(listener_check) << "I tried calling Checkbox::OnMouseClick, but it didn't call the registered listener as I expected.";;
  b.OnMouseClick(0, 0);
  ASSERT_FALSE(listener_check) << "I tried calling Checkbox::OnMouseClick twice, but it didn't call the registered listener as I expected.";;

  bool another_listener_check = false;
  b.AddListener([&](bool is_checked) {
    another_listener_check = is_checked;
  });
  b.OnMouseClick(2, 0);
  ASSERT_TRUE(listener_check) << "I tried calling Checkbox::OnMouseClick, but it didn't call the registered listener as I expected.";;
  ASSERT_TRUE(another_listener_check) << "I tried calling Checkbox::OnMouseClick, but it didn't call the registered listener as I expected.";;
}

TEST(List, Rectangle) {
  const vector<string> options {"one", "two", "three"};
  List l(43, 55, options);
  ASSERT_EQ(43, l.GetLeft());
  ASSERT_EQ(47, l.GetRight());
  ASSERT_EQ(55, l.GetTop());
  ASSERT_EQ(57, l.GetBottom());
  for (int r = -30; r <= 100; ++r) {
    for (int c = -20; c <= 100; ++c) {
      bool expected = r >= 55 && r <= 57 && c >= 43 && c <= 47;
      ASSERT_EQ(expected, l.Contains(c, r)) << "I asked a list at (43, 55) with options \"one\", \"two\", \"three\" if it contained (" << c << ", " << r << "), but I didn't get back the answer I expected.";
    }
  }

  const vector<string> options2 {"*", "**", "*******", "****", "*****************", "*", "*", "*", "*"};
  l = List(8, 107, options2);
  ASSERT_EQ(8, l.GetLeft());
  ASSERT_EQ(24, l.GetRight());
  ASSERT_EQ(107, l.GetTop());
  ASSERT_EQ(115, l.GetBottom());
  for (int r = -30; r <= 400; ++r) {
    for (int c = -20; c <= 250; ++c) {
      bool expected = r >= 107 && r <= 115 && c >= 8 && c <= 24;
      ASSERT_EQ(expected, l.Contains(c, r)) << "I asked a list at (8, 107) with options \"*\", \"**\", \"*******\", \"****\", \"*****************\", \"*\", \"*\", \"*\", \"*\" if it contained (" << c << ", " << r << "), but I didn't get back the answer I expected.";
    }
  }

}

TEST(List, Listening) {
  List l(103, 92, {"ichi", "ni", "san"});

  ASSERT_EQ(-1, l.GetSelectedIndex()) << "I asked a freshly-constructed list with options \"ichi\", \"ni\", and \"san\" what its selected index was, but I didn't get back the result I expected.";

  int listen_index = -10;
  l.AddListener([&](int i) {
    listen_index = i;
  });

  l.OnMouseClick(1, 0);
  ASSERT_EQ(0, listen_index) << "I called List::OnMouseClick with y = 0 on a list with options \"ichi\", \"ni\", and \"san\", but I didn't get back the selected index I expected through a listener I registered.";
  ASSERT_EQ(0, l.GetSelectedIndex()) << "I asked a list with options \"ichi\", \"ni\", and \"san\" what its selected index was, but I didn't get back the result I expected.";
  ASSERT_EQ("ichi", l.GetSelected()) << "I asked a list with options \"ichi\", \"ni\", and \"san\" what its selected string was, but I didn't get back the result I expected.";

  l.OnMouseClick(0, 1);
  ASSERT_EQ(1, listen_index) << "I called List::OnMouseClick with y = 1 on a list with options \"ichi\", \"ni\", and \"san\", but I didn't get back the selected index I expected through a listener I registered.";
  ASSERT_EQ(1, l.GetSelectedIndex()) << "I asked a list with options \"ichi\", \"ni\", and \"san\" what its selected index was, but I didn't get back the result I expected.";
  ASSERT_EQ("ni", l.GetSelected()) << "I asked a list with options \"ichi\", \"ni\", and \"san\" what its selected string was, but I didn't get back the result I expected.";

  l.OnMouseClick(1, 2);
  ASSERT_EQ(2, listen_index) << "I called List::OnMouseClick with y = 2 on a list with options \"ichi\", \"ni\", and \"san\", but I didn't get back the selected index I expected through a listener I registered.";
  ASSERT_EQ(2, l.GetSelectedIndex()) << "I asked a list with options \"ichi\", \"ni\", and \"san\" what its selected index was, but I didn't get back the result I expected.";
  ASSERT_EQ("san", l.GetSelected()) << "I asked a list with options \"ichi\", \"ni\", and \"san\" what its selected string was, but I didn't get back the result I expected.";

  l.OnMouseClick(1, 2);
  ASSERT_EQ(-1, listen_index) << "I called List::OnMouseClick with y = 2 (which was already selected) on a list with options \"ichi\", \"ni\", and \"san\", but I didn't get back the selected index I expected through a listener I registered.";
  ASSERT_EQ(-1, l.GetSelectedIndex()) << "I asked a list with options \"ichi\", \"ni\", and \"san\" what its selected index was, but I didn't get back the result I expected.";

  int another_listen_index;
  l.AddListener([&](int i) {
    another_listen_index = i;
  });

  l.OnMouseClick(1, 1);
  ASSERT_EQ(1, l.GetSelectedIndex()) << "I asked a list with options \"ichi\", \"ni\", and \"san\" what its selected index was, but I didn't get back the result I expected.";
  ASSERT_EQ(1, listen_index) << "I called List::OnMouseClick with y = 1 on a list with options \"ichi\", \"ni\", and \"san\", but I didn't get back the selected index I expected through a listener I registered.";
  ASSERT_EQ(1, another_listen_index) << "I called List::OnMouseClick with y = 1 on a list with options \"ichi\", \"ni\", and \"san\", but I didn't get back the selected index I expected through a listener I registered.";
}

TEST(Slider, Rectangle) {
  Slider s(23, 74, 10, 5);
  ASSERT_EQ(23, s.GetLeft());
  ASSERT_EQ(33, s.GetRight());
  ASSERT_EQ(74, s.GetTop());
  ASSERT_EQ(74, s.GetBottom());
  for (int r = -30; r <= 100; ++r) {
    for (int c = -20; c <= 100; ++c) {
      bool expected = r == 74 && c >= 23 && c <= 33;
      ASSERT_EQ(expected, s.Contains(c, r)) << "I asked a slider at (23, 74) with a maximum of 10 if it contained (" << c << ", " << r << "), but I didn't get back the answer I expected.";
    }
  }

  s = Slider(1, 99, 25, 12);
  ASSERT_EQ(1, s.GetLeft());
  ASSERT_EQ(26, s.GetRight());
  ASSERT_EQ(99, s.GetTop());
  ASSERT_EQ(99, s.GetBottom());
  for (int r = -30; r <= 100; ++r) {
    for (int c = -20; c <= 100; ++c) {
      bool expected = r == 99 && c >= 1 && c <= 26;
      ASSERT_EQ(expected, s.Contains(c, r)) << "I asked a slider at (23, 74) with a maximum of 10 if it contained (" << c << ", " << r << "), but I didn't get back the answer I expected.";
    }
  }
}

TEST(Slider, Getters) {
  Slider s(53, 23, 101, 34);
  ASSERT_EQ(34, s.GetValue());
  ASSERT_EQ(101, s.GetMax());

  s = Slider(17, 49, 80, 1);
  ASSERT_EQ(1, s.GetValue());
  ASSERT_EQ(80, s.GetMax());
}

TEST(Slider, Listening) {
  Slider s(11, 3, 8, 0);
  int listen_value = -1;
  s.AddListener([&](int i) {
    listen_value = i;
  });
  
  for (int i = 0; i <= 8; ++i) {
    s.OnMouseClick(i, 0);
    ASSERT_EQ(i, listen_value) << "I called Slider::OnMouseClick(" << i << ", 0) on a slider but a listener I registered wasn't given the value I expected.";
    ASSERT_EQ(i, s.GetValue()) << "I called Slider::GetValue on a slider after clicking on it, but I didn't get back the value I expected.";;
  }

  int another_listen_value = -1;
  s.AddListener([&](int i) {
    another_listen_value = i;
  });

  s.OnMouseClick(6, 0);
  ASSERT_EQ(6, listen_value) << "I called Slider::OnMouseClick(6, 0) on a slider but a listener I registered wasn't given the value I expected.";
  ASSERT_EQ(6, another_listen_value) << "I called Slider::OnMouseClick(6, 0) on a slider but a listener I registered wasn't given the value I expected.";
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv); 
  return RUN_ALL_TESTS();
}
